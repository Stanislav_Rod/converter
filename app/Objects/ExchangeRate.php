<?php


namespace App\Objects;


class ExchangeRate
{
    private $exchangeRate;
    private $formCurrencyCode;
    private $currencyCode;

    /**
     * @return mixed
     */
    public function getExchangeRate()
    {
        return $this->exchangeRate;
    }

    /**
     * @param mixed $exchangeRate
     * @return ExchangeRate
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCacheKey()
    {
        return $this->cacheKey;
    }

    /**
     * @param mixed $cacheKey
     * @return ExchangeRate
     */
    public function setCacheKey($cacheKey)
    {
        $this->cacheKey = $cacheKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormCurrencyCode()
    {
        return $this->formCurrencyCode;
    }

    /**
     * @param mixed $formCurrencyCode
     * @return ExchangeRate
     */
    public function setFormCurrencyCode($formCurrencyCode)
    {
        $this->formCurrencyCode = $formCurrencyCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param mixed $currencyCode
     * @return ExchangeRate
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
        return $this;
    }


}