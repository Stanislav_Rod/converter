<?php


namespace App\Objects;


class Quote
{

    /**
     * @var string $fromCurrencyCode
     */
    private $fromCurrencyCode;
    /**
     * @var int
     */
    private $amount;
    /**
     * @var string $fromCurrencyCode
     */
    private $toCurrencyCode;

    /**
     * @return string
     */
    public function getFromCurrencyCode(): string
    {
        return $this->fromCurrencyCode;
    }

    /**
     * @param string $fromCurrencyCode
     * @return Quote
     */
    public function setFromCurrencyCode(string $fromCurrencyCode): Quote
    {
        $this->fromCurrencyCode = $fromCurrencyCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return Quote
     */
    public function setAmount(int $amount): Quote
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getToCurrencyCode(): string
    {
        return $this->toCurrencyCode;
    }

    /**
     * @param string $toCurrencyCode
     * @return Quote
     */
    public function setToCurrencyCode(string $toCurrencyCode): Quote
    {
        $this->toCurrencyCode = $toCurrencyCode;
        return $this;
    }


}