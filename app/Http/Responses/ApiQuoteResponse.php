<?php

namespace App\Http\Responses;


class ApiQuoteResponse
{
    /** @var bool */
    private $success = true;

    /** @var float */
    private $exchangeRate;

    /** @var string */
    private $currencyCode;

    /** @var int */
    private $amount;

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param  bool  $success
     * @return ApiQuoteResponse
     */
    public function setSuccess(bool $success): ApiQuoteResponse
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'exchangeRate' => $this->getExchangeRate(),
            'currencyCode' => $this->getCurrencyCode(),
            'amount' => $this->getAmount(),
        ];
    }

    /**
     * @return float
     */
    public function getExchangeRate(): float
    {
        return $this->exchangeRate;
    }

    /**
     * @param  float  $exchangeRate
     * @return ApiQuoteResponse
     */
    public function setExchangeRate(float $exchangeRate): ApiQuoteResponse
    {
        $this->exchangeRate = $exchangeRate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    /**
     * @param  string  $currencyCode
     * @return ApiQuoteResponse
     */
    public function setCurrencyCode(string $currencyCode): ApiQuoteResponse
    {
        $this->currencyCode = $currencyCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param  int  $amount
     * @return ApiQuoteResponse
     */
    public function setAmount(int $amount): ApiQuoteResponse
    {
        $this->amount = $amount;
        return $this;
    }


}
