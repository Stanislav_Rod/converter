<?php

namespace App\Http\Controllers\Api;

use App\Contract\QuoteServiceInterface;
use App\Http\Requests\ApiQuoteRequest;
use App\Http\Requests\QuoteRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;


class QuoteController extends Controller
{

    /** @var QuoteServiceInterface  */
    private $quoteService;

    public function __construct(QuoteServiceInterface $quoteService)
    {
        $this->quoteService = $quoteService;
    }

    /**
     * @param  QuoteRequest  $request
     * @return JsonResponse
     */
    public function quote(QuoteRequest $request):JsonResponse
    {
        $apiRequest = ApiQuoteRequest::fromRequest($request);
        $apiQuoteResponse = $this->quoteService->getExchangeRate($apiRequest);

        return response()->json($apiQuoteResponse->toArray());
    }
}
