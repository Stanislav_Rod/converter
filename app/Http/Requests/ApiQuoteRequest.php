<?php


namespace App\Http\Requests;

/**
 * Class ApiQuoteRequest
 * @package App\Http\Requests
 */
class ApiQuoteRequest
{

    /** @var string */
    private $formCurrencyCode;

    /** @var string */
    private $currencyCode;

    /** @var int */
    private $amount;

    /**
     * @return string
     */
    public function getFormCurrencyCode(): string
    {
        return $this->formCurrencyCode;
    }

    /**
     * @param string $formCurrencyCode
     * @return ApiQuoteRequest
     */
    public function setFormCurrencyCode(string $formCurrencyCode): ApiQuoteRequest
    {
        $this->formCurrencyCode = $formCurrencyCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     * @return ApiQuoteRequest
     */
    public function setCurrencyCode(string $currencyCode): ApiQuoteRequest
    {
        $this->currencyCode = $currencyCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return ApiQuoteRequest
     */
    public function setAmount(int $amount): ApiQuoteRequest
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @param QuoteRequest $request
     * @return ApiQuoteRequest
     */
    public static function fromRequest(QuoteRequest $request): ApiQuoteRequest
    {
        return (new self())
            ->setFormCurrencyCode($request->input('fromCurrencyCode'))
            ->setAmount((int)$request->input('amount'))
            ->setCurrencyCode($request->input('toCurrencyCode'));
    }

}
