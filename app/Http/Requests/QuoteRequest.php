<?php

namespace App\Http\Requests;

use App\Enums\CurrencyEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;

class QuoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param  Request  $request
     * @return array
     */
    public function rules(Request $request)
    {
        $currencyRange = $this->getCurrencyRange();
        return [
            'fromCurrencyCode' => 'required|string|max:3|min:3|in:'.$currencyRange,
            'amount' => 'required|integer|min:1',
            'toCurrencyCode' => 'required|string|max:3|min:3|in:'.$currencyRange
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'fromCurrencyCode.in'=>'The selected to currency code is invalid. Use one of this:'.$this->getCurrencyRange(),
            'toCurrencyCode.in'=>'The selected to currency code is invalid. Use one of this:'.$this->getCurrencyRange(),
            'fromCurrencyCode.required' => 'A fromCurrencyCode is required',
            'amount.required' => 'A amount is required',
            'toCurrencyCode.required' => 'A toCurrencyCode is required',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator, response()->json($validator->errors(), 422));
    }

    /**
     * @return string
     */
    private function getCurrencyRange(): string
    {
        return implode(',', CurrencyEnum::getEnumsList());
    }
}
