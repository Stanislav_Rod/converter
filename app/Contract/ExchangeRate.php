<?php


namespace App\Contract;


use App\Http\Requests\ApiQuoteRequest;

interface ExchangeRate
{
    public function getRateExchangeRate(ApiQuoteRequest $apiQuoteRequest);
}