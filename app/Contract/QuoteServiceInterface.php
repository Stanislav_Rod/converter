<?php


namespace App\Contract;


use App\Http\Requests\ApiQuoteRequest;
use App\Http\Responses\ApiQuoteResponse;

interface QuoteServiceInterface
{
    public function getExchangeRate(ApiQuoteRequest $apiQuoteRequest): ApiQuoteResponse;
    public function getCalculateAmount(int $amount, float $rate):int;
}
