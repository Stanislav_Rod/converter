<?php


namespace App\Enums;


class CurrencyEnum extends AbstractEnum
{
    const USD = 'USD';
    const EUR = 'EUR';
    const CZK = 'CZK';

    protected static $_enums = [
        1 => self::USD,
        2 => self::EUR,
        3 => self::CZK,
    ];
}
