<?php
declare(strict_types=1);

namespace App\Enums;

abstract class AbstractEnum
{
    static protected $_enums;

    /** @var string */
    protected $_value;

    /**
     * @param $type
     */
    public function __construct($type)
    {
        if (!in_array($type, static::$_enums, true)) {
            throw new \InvalidArgumentException(sprintf('%s not in [%s]', $type, implode(',', self::getEnumsList())));
        }
        $this->_value = $type;
    }

    /**
     * @return array
     */
    public static function getEnumsList()
    {
        return array_values(static::$_enums);
    }

    /**
     * @return array
     */
    public static function getEnumsListIds()
    {
        return array_keys(static::$_enums);
    }

    /**
     * @param $value
     * @return false|int|string
     */
    public static function getIdByValue($value)
    {
        return array_search($value, static::$_enums, true);
    }

    /**
     * @param  int  $id
     * @return bool
     */
    public static function hasId(?int $id): bool
    {
        return !empty(static::$_enums[$id]);
    }

    /**
     * @param  null|string  $id
     * @return bool
     */
    public static function hasValue(?string $id): bool
    {
        return in_array($id, static::$_enums);
    }

    public static function createEnumFromId($id)
    {
        return new static(static::getValueById($id));
    }

    /**
     * @param $id
     * @return string
     */
    public static function getValueById($id)
    {
        return (string)@static::$_enums[$id];
    }

    public static function __callStatic($name, array $arguments)
    {
        return new static(constant("static::$name"));
    }

    /**
     * todo: decide equality
     * @param  AbstractEnum  $abstractEnum
     * @return bool
     */
    public function isEqual(AbstractEnum $abstractEnum)
    {
        return $this->getId() === $abstractEnum->getId();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return array_search($this->_value, static::$_enums, true);
    }

    /**
     * @param  mixed  $comparableId
     * @return bool
     */
    public function isEqualForId($comparableId)
    {
        return $this->getId() === $comparableId;
    }

    /**
     * @param  mixed  $comparableId
     * @return bool
     */
    public function isEqualForValue($comparableId)
    {
        return $this->getValue() === $comparableId;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->_value;
    }
}

