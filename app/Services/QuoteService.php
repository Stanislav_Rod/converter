<?php


namespace App\Services;


use App\Contract\ExchangeRateInterface;
use App\Contract\QuoteServiceInterface;
use App\Http\Requests\ApiQuoteRequest;
use App\Http\Responses\ApiQuoteResponse;
use Illuminate\Support\Facades\Cache;

class QuoteService implements QuoteServiceInterface
{
    /** @var float */
    const API_CACHE_MINUTES = 0.17;

    /** @var string */
    const API_CACHE_KEY = 'ExchangeRate-';

    /** @var ExchangeRateInterface */
    private $exchangeRateService;

    public function __construct(ExchangeRateInterface $exchangeRateService)
    {
        $this->exchangeRateService = $exchangeRateService;
    }

    /**
     * @param  ApiQuoteRequest  $apiQuoteRequest
     * @return ApiQuoteResponse
     */
    public function getExchangeRate(ApiQuoteRequest $apiQuoteRequest): ApiQuoteResponse
    {
        $quoteResponse =
            Cache::remember(
                $this->getCacheKey($apiQuoteRequest),
                self::API_CACHE_MINUTES,
                function () use ($apiQuoteRequest) {
                    return $this->exchangeRateService->getRateExchangeRate($apiQuoteRequest);
                }
            );
        $quoteResponse->setAmount(
            $this->getCalculateAmount($apiQuoteRequest->getAmount(), $quoteResponse->getExchangeRate())
        );
        return $quoteResponse;
    }

    /**
     * @param  ApiQuoteRequest  $apiQuoteRequest
     * @return string
     */
    private function getCacheKey(ApiQuoteRequest $apiQuoteRequest): string
    {
        return self::API_CACHE_KEY.$apiQuoteRequest->getCurrencyCode().$apiQuoteRequest->getFormCurrencyCode();
    }

    public function getCalculateAmount(int $amount, float $rate): int
    {
        return floor(($amount * $rate) * 100);
    }
}
