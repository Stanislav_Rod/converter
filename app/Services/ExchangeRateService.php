<?php


namespace App\Services;


use App\Contract\ExchangeRateInterface;
use App\Http\Requests\ApiQuoteRequest;
use App\Http\Responses\ApiQuoteResponse;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class ExchangeRateService
 * @package App\Services
 */
class ExchangeRateService implements ExchangeRateInterface
{
    /** @var string */
    private $baseUri;

    /** @var Client */
    private $httpClient;

    /**
     * ExchangeRateService constructor.
     * @param  Client  $httpClient
     * @param $baseUri
     */
    public function __construct(Client $httpClient, $baseUri)
    {
        $this->httpClient = $httpClient;
        $this->baseUri = $baseUri;
    }

    /**
     * @param  ApiQuoteRequest  $apiQuoteRequest
     * @return ApiQuoteResponse
     * @throws GuzzleException
     */
    public function getRateExchangeRate(ApiQuoteRequest $apiQuoteRequest): ApiQuoteResponse
    {
        $response = new ApiQuoteResponse();
        try {
            $apiResponse = $this->apiRequest($apiQuoteRequest);
            $data = json_decode($apiResponse->getBody()->getContents(), true);
        } catch (RequestException $exception) {
            $response->setSuccess(false);
        }
        return $response
            ->setExchangeRate($data['rates'][$apiQuoteRequest->getCurrencyCode()])
            ->setAmount($apiQuoteRequest->getAmount())
            ->setCurrencyCode($apiQuoteRequest->getCurrencyCode());
    }

    /**
     * @param  ApiQuoteRequest  $request
     * @return ResponseInterface
     * @throws GuzzleException
     */
    private function apiRequest(ApiQuoteRequest $request): ResponseInterface
    {
        try {
            return $this->httpClient->get(
                $this->baseUri,
                [
                    'query' => [
                        'base' => $request->getFormCurrencyCode(),
                        'symbols' => $request->getCurrencyCode()
                    ]
                ]
            );
        } catch (RequestException $e) {
            if ($e->getResponse() && $e->getResponse()->getStatusCode() === 400) {
                $errorMessage = json_decode($e->getResponse()->getBody()->__toString(), true);
                $errorString = $errorMessage['error'];
                throw new BadRequestHttpException($errorString);
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
