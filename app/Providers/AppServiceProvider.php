<?php

namespace App\Providers;

use App\Contract\ExchangeRateInterface;
use App\Contract\QuoteServiceInterface;
use App\Services\ExchangeRateService;
use App\Services\QuoteService;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(QuoteServiceInterface::class,QuoteService::class);
        $this->app->bind(ExchangeRateInterface::class, function ($app) {
            return new ExchangeRateService(new Client(), config('exchangerate.uri'));
        });
    }
}
